package com.epam.learn.l2;

public class StaticInitial {
//    private final static int count;
    private int count1;
    private Cat cat;

    public StaticInitial(int count1, Cat cat){
        this.count1 = count1;
        this.cat = cat;
    }

    public String getName(){
        return cat.getName();
    }
//    static{
//        count =5;
//    }
////    private StaticInitial(int count){
////        this.count = count;
////    }
//    public static void main(String[] args) {
////        StaticInitial in = new StaticInitial(2);
//        System.out.println(StaticInitial.count);
//    }
}
