package com.epam.learn.l2;

public class Meduza extends Animal {
    @Override
    public void eat() {
        System.out.println("His bode is his mouth");
    }

    public static void main(String[] args) {
        Animal meduza = new Meduza();
        meduza.eat();
    }
}
