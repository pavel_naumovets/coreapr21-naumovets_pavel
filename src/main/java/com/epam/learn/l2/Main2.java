package com.epam.learn.l2;

public class Main2 {
    public static void main(String[] args) {
        int param1 = 10;
        int param2 = 10;
        System.out.println(++param1);
        System.out.println(param2++);
        Main2 main = new Main2();
        System.out.println(main.isEven(4));
        System.out.println(main.isOdd(5));
    }
    private boolean isEven(int param) {
        return param % 2 == 0;
    }
    private boolean isOdd(int param){
        return param %2 != 0;
    }
}
