package com.epam.learn.l4;

public class Cat extends Animal {

    public Cat(String name, int age){
        super(name, age);
    }
//    @Override
//    public String toString(){
//        return "Cat{" + "name '" + name + '\'' + '}';
//    }
    @Override
    public String toString(){
        return super.getName() + " " + super.getAge();
    }
}
