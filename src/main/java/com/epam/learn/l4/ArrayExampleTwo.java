package com.epam.learn.l4;

import java.util.Arrays;

public class ArrayExampleTwo {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik", 2);
        Cat murzik = new Cat("murzik", 2);

        Cat[] catsArray = new Cat[2];
        catsArray[0] = barsik;
        catsArray[1] = murzik;

        Cat[] cats = new Cat[]{barsik, murzik};
        System.out.println(Arrays.toString(cats));
    }
}
