package com.epam.learn.l4;

public class Main4 {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsic", 2);
        Cat murzik = new Cat("murzik", 2);
        Cat pushok = new Cat("pushok", 2);
        Dog sharik = new Dog("Sharik", 7);

//        Cat[] cats = new Cat[]{barsik, murzik, pushok};
        Animal[] animals = new Animal[]{barsik, murzik, pushok, sharik};

        for(Animal animal : animals){
//            Cat cat = (Cat) animal;
            System.out.println(animal.toString());
        }
    }
}
