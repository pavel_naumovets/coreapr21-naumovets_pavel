package com.epam.learn.l3;

import java.io.*;
import java.util.Objects;
public class Main {
    private static boolean isTrue = true;
    //if
    public static void main(String[] args) {
        //reader - позволяет считывать с консоли что-то
//        String line = null;
//        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
//            // readLine - считать линию;
//            line = reader.readLine();
//        } catch(IOException e){
//            e.printStackTrace();
//        }

//        int finalResult = Integer.parseInt(Objects.requireNonNull(line));
//
//        //в такой конструкции если приходит значение больше 3 - у нас красный
//        if(finalResult == 1){
//            System.out.println("grean");
//        } else if(finalResult == 2){
//            System.out.println("yellow");
//        } else {
//            System.out.println("red");
//        }

        // Object сравниваются через equals
        Colours colours = Colours.GREEN;
        if(getColourByTime(colours) == Colours.GREEN){
            System.out.println("GREEN");
        } else if(getColourByTime(colours) == Colours.YELLOW){
            System.out.println("YELLOW");
        } else if(getColourByTime(colours) == Colours.RED){
            System.out.println("RED");
        } else {
            throw new RuntimeException();
        }
    }

    private static Colours getColourByTime(Colours colours){
        return colours;
    }
}
