package com.epam.learn.l1;

public class Main2 {
    public static void main(String[] args) {
        double v1 = 2.0;
        float f1 = 2.0f;
        float f2 = (float) v1;
        double v2 = f2;
        System.out.println(f2);
        System.out.println(v1 == v2);
        float v = 1/2.3f;
        int v3 = (int) (1/2.3f);
    }
}
