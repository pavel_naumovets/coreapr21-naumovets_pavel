package com.epam.learn.l1;

public class DefaultPrimitive {
    public static void main(String[] arr){
        System.out.println(getSum((byte) 1, (byte) 2));
    }

    public static byte getSum(byte a, byte b){
        a = 127;
        b = 1;
        return (byte) (a+b);
    }
    public static long getIntSum(int a, long b){
        return (long) (a+b);
    }
    public double getSomething(float a, double b){
        double something = a;
        float f = (float)b;
        return a+b;
    }
}
