package com.epam.learn.l5;

public class Main3 {
    public static void main(String[] args) {
        String example = "barsik"; // одинаковыя надпись с String example4 = new String("barsik").intern();
        String example3 = "barsik";// один и тотже объект,т.к. String_pool
        String example2 = "bar" + "sik";
        String example4 = new String("barsik").intern();

//        System.out.println(example.concat(example2));

        System.out.println(example == "barsik");
        System.out.println(example == example3);
        System.out.println(example == example2);
        System.out.println(example == example4);

    }
}
