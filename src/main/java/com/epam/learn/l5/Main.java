package com.epam.learn.l5;

import java.io.UnsupportedEncodingException;

public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String s = "this is string";
        String s1 = "";
        String s2 = null;
        s = "sfdjgjdgf"; // создается новый объект
        char[] charArray = s.toCharArray();
        String s3 = new String(charArray, 0, 3);
        String s4 = new String(charArray);
        System.out.println(s4);

        byte[] ascii = {97, 66, 67, 68, 69, 70};
        System.out.println(new String(ascii));

        byte[] data = {(byte) 0xE3, (byte) 0xEE};
        System.out.println(new String(data, "CP866"));
    }
}
