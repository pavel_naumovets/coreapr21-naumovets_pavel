package com.epam.learn.l5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main4 {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String param = reader.readLine();
            if (reader.readLine().equals("1")) {
                new Main4().fromLiteral();
            } else {
                new Main4().fromLiteral2();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fromLiteral() {
        String name = new String("barsik").intern();
        System.out.println(name == "barsik");
    }
    private void fromLiteral2() {
        String name = "murzik";
        System.out.println(name == "murzik");
    }
}
